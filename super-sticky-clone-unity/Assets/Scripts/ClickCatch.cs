﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

[RequireComponent(typeof(EventTrigger))]
public class ClickCatch : MonoBehaviour
{
    public Action eventClick;
    EventTrigger eventTrigger;
    
    void Start()
    {
        SetupListeners();
    }
    
    void SetupListeners()
    {
        eventTrigger = gameObject.GetComponent<EventTrigger>();

        var pointerDown = new EventTrigger.TriggerEvent();

        pointerDown.AddListener(data =>
        {
            if (eventClick != null)
            {
                eventClick.Invoke();
            }
        });
        
        eventTrigger.triggers.Add(new EventTrigger.Entry { callback = pointerDown, eventID = EventTriggerType.PointerDown });
    }
}