﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ColliderDetection : MonoBehaviour {

    public Action<GameObject> eventDetection;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (eventDetection != null)
        {
            eventDetection(collider.gameObject);
        }
    }
}
