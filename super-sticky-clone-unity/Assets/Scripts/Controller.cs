﻿using UnityEngine;
using DG.Tweening;
using System;

public enum TypeControllerComplete { None, ToLeft, ToRight, Fall }
public class Controller : MonoBehaviour {

    float OFFSET_JUMP_Y = 2f;
    const float TIME_JUMP = 0.5f;
    const float TIME_DELAY_BEFORE_FALL = 0.25f;
    const float SPEED_FALL = 3f;
    const float HORIZONTAL_OFFSET = 0.06f;
    const float VERTICAL_OFFSET = 0.075f;

    Level level;
    public Action<TypeControllerComplete> eventComplete;
    Tweener tweener;
    Sequence seq;

    public void Init(Level level) {
        this.level = level;
        OFFSET_JUMP_Y = level.width / 2f;
    }

    #region SET SPAWN (Start position)
    public void SetSpawn()
    {
        TweenClear();
        transform.localPosition = level.nodeSpawn.localPosition + HORIZONTAL_OFFSET * Vector3.right;
    }
    #endregion
    #region JUMP
    public void JumpToRight()
    {
        TweenClear();
        bool isLeftSide = false;
        seq.Append(Jump_SideCase(isLeftSide));
        seq.Join(Jump_UpCase());
        seq.AppendCallback(Jump_CompleteCase(isLeftSide));
    }

    public void JumpToLeft()
    {
        TweenClear();
        bool isLeftSide = true;
        seq.Append(Jump_SideCase(isLeftSide));
        seq.Join(Jump_UpCase());
        seq.AppendCallback(Jump_CompleteCase(isLeftSide));
    }

    Tweener Jump_SideCase(bool isLeftSide) {
        float value = (
            isLeftSide
            ? level.nodeLeft.localPosition.x + HORIZONTAL_OFFSET
            : level.nodeRight.localPosition.x - HORIZONTAL_OFFSET
            );
        return transform.DOLocalMoveX(
            value,
            TIME_JUMP
            );
    }

    Tweener Jump_UpCase() {
        return transform.DOLocalMoveY(
            Mathf.Clamp(
                transform.localPosition.y + OFFSET_JUMP_Y,
                level.nodeDown.localPosition.y,
                level.nodeUp.localPosition.y - VERTICAL_OFFSET
                ),
            TIME_JUMP
            ).SetEase(Ease.Linear)
            ;
    }

    TweenCallback Jump_CompleteCase(bool isLeftSide) {
        return () => {
            if (isLeftSide)
            {
                CompleteJumpToLeft();
            }
            else {
                CompleteJumpToRight();
            }
        };
    }
    #endregion
    #region FALL
    public void Fall() {
        TweenClear();
        seq.AppendInterval(TIME_DELAY_BEFORE_FALL);
        seq.Append(Fall_MoveDownAccelerate());
        seq.Append(Fall_MoveDown());
        seq.AppendCallback(Fall_Complete());
    }
    TweenCallback Fall_Complete() {
        return () => {
            CompleteFall();
        };
    }
    Tweener Fall_MoveDownAccelerate()
    {
        float delta_move_y = level.height / 5f;
        float point_to_accelerate = transform.localPosition.y - delta_move_y;
        return transform
            .DOLocalMoveY(point_to_accelerate, delta_move_y * 2f / SPEED_FALL).SetEase<Tweener>(Ease.InQuad);
    }
    Tweener Fall_MoveDown() {
        float timeFall = (transform.localPosition.y - level.nodeDown.localPosition.y) / SPEED_FALL;
        return transform
            .DOLocalMoveY(level.nodeDown.localPosition.y, timeFall);
    }
    #endregion
    #region TweenClear
    void TweenClear()
    {
        seq.Kill();
        seq = DOTween.Sequence();
    }
    #endregion
    #region COMPLETE, EVENT
    public void Complete(TypeControllerComplete complete) {
        if (eventComplete != null)
        {
            eventComplete.Invoke(complete);
        }
    }

    void CompleteJumpToLeft() {
        Complete(TypeControllerComplete.ToLeft);
    }

    void CompleteJumpToRight()
    {
        Complete(TypeControllerComplete.ToRight);
    }

    void CompleteFall() {
        Complete(TypeControllerComplete.Fall);
    }
    #endregion
}
