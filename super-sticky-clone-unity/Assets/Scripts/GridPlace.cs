﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridPlace {

    int col, row;
    float border_up, border_down, border_left, border_right;
    float x_center, y_center;
    float widthCell, heightCell;
    float widthPlace, heightPlace;
    float halfWidthCell, halfHeightCell;

    public void CreateGrid(int col, int row, float border_left, float border_right, float border_down, float border_up
        ) {
        this.col = col;
        this.row = row;
        this.border_up = border_up;
        this.border_down = border_down;
        this.border_left = border_left;
        this.border_right = border_right;
        x_center = (border_right + border_left) * .5f;
        y_center = (border_up + border_down) * .5f;
        widthPlace = Mathf.Abs(border_right - border_left);
        heightPlace = Mathf.Abs(border_up - border_down);
        widthCell = widthPlace / ((float)col);
        heightCell = heightPlace / ((float)row);
        halfWidthCell = .5f * widthCell;
        halfHeightCell = .5f * heightCell;
    }
    public float width
    {
        get
        {
            return widthPlace;
        }
    }
    public float height
    {
        get
        {
            return heightPlace;
        }
    }
    public Vector2 GetCoordPoint(Vector2Int point) {
        Vector2 vec = Vector2.zero;
        float x = x_center - .5f * widthPlace + halfWidthCell + ((float)(col - point.x - 1)) * widthCell;
        float y = y_center - .5f * heightPlace + halfHeightCell + ((float)(row - point.y - 1)) * heightCell;
        vec.Set(x, y);
        return vec;
    }
}
