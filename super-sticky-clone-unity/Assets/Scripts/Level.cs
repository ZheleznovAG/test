﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

    float offset = 0.1f;
    public Transform nodeUp, nodeDown, nodeLeft, nodeRight, nodeSpawn;
    public int col = 5;
    public int row = 7;

    [SerializeField] Place place;

    void Awake()
    {
        place.Init(col, row, nodeLeft.localPosition.x, nodeRight.localPosition.x, nodeDown.localPosition.y, nodeUp.localPosition.y);

    }
    public float width {
        get
        {
            return place.width;
        }
    }
    public float height
    {
        get
        {
            return place.height;
        }
    }
    public void SetPositionRespawnItem(GameObject go) {
        place.SetPositionRespawnItem(go);
    }

    public void GO_Ingame(GameObject go) {
        place.GO_Ingame(go);
    }
    public void GO_Outgame(GameObject go) {
        place.GO_Outgame(go);
    }
}
