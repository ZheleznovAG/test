﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerGame : MonoBehaviour
{
    const string STR_ACID = "ACID";
    const string STR_TAKEITEM = "TakeItem";
    const int COUNT_FLY = 5;

    [SerializeField] ClickCatch clickCatch;
    [SerializeField] Player player;
    [SerializeField] Level level;
    [SerializeField] ManagerItems managerItems;

	void Start () {
        Init();
        ReSpawnPlayer();
    }

    void Init() {
        clickCatch.eventClick += ActionOnEventClick;
        CreatePlayer();
        CreateFly();
    }

    #region Fly Logic
    void CreateFly() {
        for (int i = 0; i < (5-2)*(7-2); i++)
        {
            GameObject go = managerItems.GetFly().gameObject;
            level.GO_Ingame(go);
            level.SetPositionRespawnItem(go);
        }
    }

    void TakeFly(GameObject go) {
        Debug.Log("TakeFly");
        level.GO_Outgame(go);
        managerItems.HideFly(go);
    }
    #endregion
    #region Player Logic
    void CreatePlayer()
    {
        player = managerItems.GetPlayer();
        player.eventTakeItem += ActionOnEventPlayerTakeItem;
        //player.eventCompleteMoveToACID += DiePlayer;
        player.eventJumpSideComplete += StartFall;
        player.Init(level);
        level.GO_Ingame(player.gameObject);
    }

    void ActionOnEventClick() {
        player.Jump();
    }
    void ActionOnEventPlayerTakeItem(GameObject go) {
        Debug.Log("ActionOnEventPlayerTakeItem: " + go.tag);
        switch (go.tag)
        {
            case STR_ACID:
                DiePlayer();
                break;
            case STR_TAKEITEM:
                TakeFly(go.transform.parent.gameObject);
                break;
            default:
                break;
        }
    }

    void DiePlayer()
    {
        ReSpawnPlayer();
    }
    
    void ReSpawnPlayer()
    {
        SetStartPosition();
        StartFall();
    }
    
    void SetStartPosition() {
        player.SetSpawn();
    }

    void StartFall() {
        player.Fall();
    }
    #endregion
}
