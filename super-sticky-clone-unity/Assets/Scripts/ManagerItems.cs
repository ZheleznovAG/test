﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerItems : MonoBehaviour {

    [SerializeField] GameObject examplePlayer, exampleFly;
    GameObject goPlayer;
    List<GameObject> listFlyOutgame;
    List<GameObject> listFlyIngame;

    public Player GetPlayer() {
        if (goPlayer == null)
        {
            goPlayer = (GameObject)Instantiate(examplePlayer);
        }
        return goPlayer.GetComponent<Player>();
    }
    void CheckListNull(ref List<GameObject> list)
    {
        if (list == null)
        {
            list = new List<GameObject>();
        }
    }

    public TakeItem GetFly()
    {
        GameObject go = null;

        CheckListNull(ref listFlyOutgame);
        CheckListNull(ref listFlyIngame);
        if (listFlyOutgame.Count > 0)
        {
            int index = listFlyOutgame.Count - 1;
            go = listFlyOutgame[index];
            listFlyIngame.Add(go);
            listFlyOutgame.Remove(go);
        }
        else {
            go = (GameObject)Instantiate(exampleFly);
            listFlyIngame.Add(go);
        }

        return go.GetComponent<TakeItem>();
    }
    public void HideFly(GameObject _go) {
        int index = listFlyIngame.IndexOf(_go);
        GameObject go = listFlyIngame[index];
        listFlyOutgame.Add(go);
        listFlyIngame.Remove(go);
    }
}
