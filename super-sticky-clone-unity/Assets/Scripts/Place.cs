﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Place : MonoBehaviour {


    const float OFFSET_OUTSIDE = 100f;

    GridPlace gridPlace;

    public float width
    {
        get
        {
            return gridPlace.width;
        }
    }
    public float height
    {
        get
        {
            return gridPlace.height;
        }
    }
    int col = 1;
    int row = 1;

    public void Init(int col, int row, float border_left, float border_right, float border_down, float border_up)
    {
        this.col = col;
        this.row = row;
        CreateListRespawn();
        gridPlace = new GridPlace();
        gridPlace.CreateGrid(col, row, border_left, border_right, border_down, border_up);
    }
    List<Vector2Int> listPointRespawn;

    void CreateListRespawn() {
        listPointRespawn = new List<Vector2Int>();
        for (int i = 1; i < row - 1; i++)
        {
            for (int j = 1; j < col - 1; j++)
            {
                listPointRespawn.Add(new Vector2Int(j, i));
            }
        }
    }

    public void GO_Ingame(GameObject go) {
        go.SetActive(true);
        go.transform.SetParent(transform);
        go.transform.localPosition = Vector3.zero;
        go.transform.localScale = Vector3.one;
        go.transform.localEulerAngles = Vector3.zero;
    }
    public void GO_Outgame(GameObject go)
    {
        go.SetActive(false);
        go.transform.localPosition = Vector3.up * OFFSET_OUTSIDE;
    }
    public void SetPositionRespawnItem(GameObject go) {
        int index_random = Random.Range(0, listPointRespawn.Count);
        go.transform.localPosition = gridPlace.GetCoordPoint(listPointRespawn[index_random]);
        listPointRespawn.RemoveAt(index_random);
    }
}
