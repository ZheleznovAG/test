﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Controller))]
public class Player : MonoBehaviour {

    public Action eventCompleteMoveToACID;
    public Action eventJumpSideComplete;
    public Action<GameObject> eventTakeItem;

    Controller controller;
    [SerializeField] ViewPlayer view;
    [SerializeField] ColliderDetection colliderDetection;

    bool IsMove { get; set; }
    bool isLeftSide = true;
    bool IsLeftSide {
        get {
            return isLeftSide;
        }
        set {
            if (value != isLeftSide)
            {
                isLeftSide = value;
                view.IsLeftSide = value;
            }
        }
    }

    public void Init(Level level) {
        controller = GetComponent<Controller>();
        controller.Init(level);
        controller.eventComplete += ActionOnEventControllerComplete;
        colliderDetection.eventDetection += ActionOnEventCollisionTakeItem;
    }

    #region Take
    void ActionOnEventCollisionTakeItem(GameObject go) {
        if (eventTakeItem != null)
        {
            eventTakeItem.Invoke(go);
        }
    }
    #endregion
    #region Controller Action
    public void Jump() {
        if (!IsMove) {
            IsMove = true;
            if (IsLeftSide) {
                JumpToRight();
            }
            else {
                JumpToLeft();
            }
        }
    }

    public void SetSpawn() {
        IsMove = false;
        IsLeftSide = true;
        controller.SetSpawn();
    }

    void JumpToRight() {
        controller.JumpToRight();
    }

    void JumpToLeft()
    {
        controller.JumpToLeft();
    }

    public void Fall()
    {
        controller.Fall();
    }
    #endregion
    #region COMPLETE, EVENT, ACTION_ON_EVENT
    void ActionOnEventControllerComplete(TypeControllerComplete complete)
    {
        switch (complete)
        {
            case TypeControllerComplete.None:
                break;
            case TypeControllerComplete.ToLeft:
                CompleteJumpToLeft();
                break;
            case TypeControllerComplete.ToRight:
                CompleteJumpToRight();
                break;
            case TypeControllerComplete.Fall:
                CompleteFall();
                break;
            default:
                break;
        }
    }
    void CompleteJumpToRight()
    {
        CompleteJumpSide();
        IsMove = false;
        IsLeftSide = false;
    }
    void CompleteJumpToLeft()
    {
        CompleteJumpSide();
        IsMove = false;
        IsLeftSide = true;
    }

    void CompleteJumpSide() {
        if (eventJumpSideComplete != null)
        {
            eventJumpSideComplete.Invoke();
        }
    }

    void CompleteFall() {
        if (eventCompleteMoveToACID != null)
        {
            eventCompleteMoveToACID.Invoke();
        }
    }
    #endregion
}
