﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum TypeTakeItem { None, Fly }
public class TakeItem : MonoBehaviour {

    TypeTakeItem type;
    public virtual TypeTakeItem Type {
        protected set {
            type = value;
        }
        get {
            return type;
        }
    }

    void Start()
    {
        Init();
    }

    public virtual void Init() {

    }
}
