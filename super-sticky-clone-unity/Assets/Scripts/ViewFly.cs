﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ViewFly : MonoBehaviour {
    [SerializeField] List<Sprite> sprites;
    [SerializeField] SpriteRenderer spriteRenderer;

    Sequence seq;
    const float TIMER = 0.1f;

    void Start()
    {
        Init();
    }

    public void Init() {
        seq = DOTween.Sequence();
        foreach (var item in sprites)
        {
            seq.AppendInterval(TIMER);
            seq.AppendCallback(() => {
                spriteRenderer.sprite = item;
            });
        }
        seq.SetLoops<Sequence>(-1);
    }


}
