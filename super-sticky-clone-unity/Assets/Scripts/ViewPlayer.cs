﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewPlayer : MonoBehaviour {
    [SerializeField] Sprite sprite;
    [SerializeField] SpriteRenderer spriteRenderer;
    bool isLeftSide = true;
    public bool IsLeftSide
    {
        get { return isLeftSide; }
        set
        {
            if (value != isLeftSide)
            {
                isLeftSide = value;
                spriteRenderer.flipX = value;
            }
        }
    }
}
